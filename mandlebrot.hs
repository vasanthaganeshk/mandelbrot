import Data.Complex
import Data.List (concat)
import Graphics.GD
import qualified Control.Monad.Parallel as P
import Data.Bits

w = 2000::Int
h = 2000::Int

pxls = [(i,j) | i <- [1..2000], j <- [1..2000]]

createZ (x,y) = zx:+zy
  where zx = 1.5*(xi - 3.0*wi/4.0)/(0.5*wi) :: Double
        zy = 1.0*(yi - hi/2.0)/(0.5*hi) :: Double
        hi = fromIntegral h
        wi = fromIntegral w
        xi = fromIntegral x
        yi = fromIntegral y

zs = map createZ pxls

pixelCol z = fromIntegral $ i `shiftL` 21 + i `shiftL` 10 + i*8
  where i = length . takeWhile (<4.0) . map magnitude $ scanl (\x y -> (x*x) + z) (0:+0) [1..256]

main = do
  b <- newImage (w, h)
  P.mapM_ (\(pt, col)-> setPixel pt col b) $ zip pxls (map pixelCol zs)
  savePngFile "/home/homonculus/parallel_mandlebrot.png" b